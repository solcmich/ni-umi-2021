(define (domain lloyd)
(:predicates (curr_pos ?p ?x ?y) (adj_inc ?p ?pp) (adj_dec ?pp ?p) (free ?x ?y))
(:action move_up
        :parameters (?p ?x ?y ?x_new)
        :precondition (and (curr_pos ?p ?x ?y) (free ?x_new ?y) (adj_dec ?x ?x_new))
        :effect (and (not (curr_pos ?p ?x ?y)) (free ?x ?y) (not (free ?x_new ?y)) (curr_pos ?p ?x_new ?y))
)
(:action move_down
        :parameters (?p ?x ?y ?x_new)
        :precondition (and (curr_pos ?p ?x ?y) (free ?x_new ?y) (adj_inc ?x ?x_new))
        :effect (and (not (curr_pos ?p ?x ?y)) (free ?x ?y) (not (free ?x_new ?y)) (curr_pos ?p ?x_new ?y))
)
    
(:action move_right
        :parameters (?p ?x ?y ?y_new)
        :precondition (and (curr_pos ?p ?x ?y) (free ?x ?y_new) (adj_inc ?y ?y_new))
        :effect (and (not (curr_pos ?p ?x ?y)) (free ?x ?y) (not (free ?x ?y_new))  (curr_pos ?p ?x ?y_new))
)
    
(:action move_left
        :parameters (?p ?x ?y ?y_new)
        :precondition (and (curr_pos ?p ?x ?y) (adj_dec ?y ?y_new) (free ?x ?y_new))
        :effect (and (not (curr_pos ?p ?x ?y)) (free ?x ?y) (not (free ?x ?y_new)) (curr_pos ?p ?x ?y_new))
)
)