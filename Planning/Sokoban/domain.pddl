(define (domain sokoban)

    (:predicates (wall ?x ?y) (box ?x ?y) (curr_pos ?x ?y) (adj_inc ?p ?pp) (adj_dec ?pp ?p))


    (:action box_up
        :parameters (?x ?y ?x_new ?b_x_new)
        :precondition (and (curr_pos ?x ?y) (box ?x_new ?y) (not (wall ?x_new ?y)) (adj_dec ?x ?x_new) (not (wall ?b_x_new ?y)) (not (box ?b_x_new ?y)) (adj_dec ?x_new ?b_x_new))
        :effect (and (not (curr_pos ?x ?y)) (not (box ?x_new ?y)) (curr_pos ?x_new ?y) (box ?b_x_new ?y))
    )
    (:action box_down
        :parameters (?x ?y ?x_new ?b_x_new)
        :precondition (and (curr_pos ?x ?y) (box ?x_new ?y) (not (wall ?x_new ?y))  (adj_inc ?x ?x_new) (not (wall ?b_x_new ?y)) (not (box ?b_x_new ?y)) (adj_inc ?x_new ?b_x_new))
        :effect (and (not (curr_pos ?x ?y)) (not (box ?x_new ?y)) (curr_pos ?x_new ?y)  (box ?b_x_new ?y))
    )
    
    (:action box_right
        :parameters (?x ?y ?y_new ?b_y_new)
        :precondition (and (curr_pos ?x ?y) (box ?x ?y_new) (not (wall ?x ?y_new))  (adj_inc ?y ?y_new) (not (wall ?x ?b_y_new)) (not (box ?x ?b_y_new)) (adj_inc ?y_new ?b_y_new))
        :effect (and (not (curr_pos ?x ?y)) (not (box ?x ?y_new)) (curr_pos ?x ?y_new) (box ?x ?b_y_new))
    )
    
    (:action box_left
        :parameters (?x ?y ?y_new ?b_y_new)
        :precondition (and (curr_pos ?x ?y) (box ?x ?y_new) (not (wall ?x ?y_new)) (adj_dec ?y ?y_new) (not (wall ?x ?b_y_new)) (not (box ?x ?b_y_new)) (adj_dec ?y_new ?b_y_new))
        :effect (and (not (curr_pos ?x ?y)) (not (box ?x ?y_new)) (curr_pos ?x ?y_new)  (box ?x ?b_y_new))
    )
    
    (:action move_up
        :parameters (?x ?y ?x_new)
        :precondition (and (curr_pos ?x ?y) (not (wall ?x_new ?y)) (not (box ?x_new ?y)) (adj_dec ?x ?x_new))
        :effect (and (not (curr_pos ?x ?y)) (curr_pos ?x_new ?y))
    )
    (:action move_down
        :parameters (?x ?y ?x_new)
        :precondition (and (curr_pos ?x ?y) (not (wall ?x_new ?y)) (not (box ?x_new ?y)) (adj_inc ?x ?x_new))
        :effect (and (not (curr_pos ?x ?y)) (curr_pos ?x_new ?y))
    )
    
    (:action move_right
        :parameters (?x ?y ?y_new)
        :precondition (and (curr_pos ?x ?y) (not (wall ?x ?y_new)) (not (box ?x ?y_new)) (adj_inc ?y ?y_new))
        :effect (and (not (curr_pos ?x ?y)) (curr_pos ?x ?y_new))
    )
    
    (:action move_left
        :parameters (?x ?y ?y_new)
        :precondition (and (curr_pos ?x ?y) (not (wall ?x ?y_new)) (not (box ?x ?y_new)) (adj_dec ?y ?y_new))
        :effect (and (not (curr_pos ?x ?y)) (curr_pos ?x ?y_new))
    )

    
)