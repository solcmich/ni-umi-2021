(define (problem test)
    (:domain sokoban)
    (:objects v1 v2 v3 v4 v5 v6 v7 v8 v9)
   
    (:init 
    ;define adjancency
    (adj_inc v1 v2) (adj_inc v2 v3) (adj_inc v3 v4) (adj_inc v4 v5) (adj_inc v5 v6)
           (adj_inc v6 v7) (adj_inc v7 v8) (adj_inc v8 v9)
           
           (adj_dec v9 v8) (adj_dec v8 v7) (adj_dec v7 v6) (adj_dec v6 v5) (adj_dec v5 v4) 
           (adj_dec v4 v3) (adj_dec v3 v2) (adj_dec v2 v1)
           
            ;objects
            (wall v1 v3)  (wall v1 v4) (wall v1 v5) (wall v1 v6) (wall v1 v7) 
            (wall v2 v1)  (wall v2 v2) (wall v2 v3) (wall v2 v7) 
            (wall v3 v1) (wall v3 v7) 
            (wall v4 v1) (wall v4 v2) (box v4 v3) (box v4 v7)
            (wall v5 v1) (wall v5 v3) (box v5 v4) (box v5 v7)
            (wall v6 v1) (wall v6 v3) (box v6 v7) (box v6 v8)
            (wall v7 v1) (wall v7 v8) 
            (wall v8 v1) (wall v8 v8) 
            (wall v9 v1) (wall v9 v2) (wall v9 v3) (wall v9 v4) (wall v9 v5) (wall v9 v6) (wall v9 v7) (wall v9 v8) 
            
            (box v3 v4) (box v4 v5) (box v5 v5) (box v7 v2) (box v7 v4) (box v7 v5) (box v7 v6)

            (curr_pos v3 v3))

    (:goal (and (box v3 v2) (box v4 v6) (box v5 v2) (box v6 v5) (box v7 v7) (box v8 v5) (box v7 v4)))
)